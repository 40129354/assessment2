﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{
    class Staff
    {
        private int id;

        public int Id
        {
            get { return id; }

            set { id = value; }
        }
    }

    class Server : Staff
    {
        private string name;

        public string Name
        {
            get { return name; }

            set { name = value; }
        }
    }

    class Driver : Staff
    {
        private string carRegistration;
        private string driverName;

       
        public string CarRegistration
        {
            get { return carRegistration; }

            set { carRegistration = value; }
        }

        public string DriverName
        {
            get { return driverName; }

            set { driverName = value; }
        }
    }
}
