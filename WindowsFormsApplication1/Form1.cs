﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public List<string> orders = new List<string>();
        public Form1()
        {
            InitializeComponent();


            // Create dish, servers and drivers when Form1 is called taking the validations from the classes.
            Dish tomatoPizza = new Dish();
            tomatoPizza.Description = "Tomato Pizza";
            tomatoPizza.Price = 450;
            tomatoPizza.IsVegetarian = true;

            Dish peperoniPizza = new Dish();
            peperoniPizza.Description = "Peperoni Pizza";
            peperoniPizza.IsVegetarian = false;
            peperoniPizza.Price = 500;

            Dish bolognaise = new Dish();
            bolognaise.Description = "Spaghetti Bolognaise";
            bolognaise.IsVegetarian = false;
            bolognaise.Price = 600;

            Dish tomatoPasta = new Dish();
            tomatoPasta.Description = "Tomato Pasta";
            tomatoPasta.IsVegetarian = true;
            tomatoPasta.Price = 350;

            Server server_Ben = new Server();
            server_Ben.Id = 1;
            server_Ben.Name = "Ben";

            Server server_Louise = new Server();
            server_Louise.Id = 2;
            server_Louise.Name = "Louise";

            Server server_Neil = new Server();
            server_Neil.Id = 3;
            server_Neil.Name = "Neil";

            Driver driver_Jim = new Driver();
            driver_Jim.Id = 4;
            driver_Jim.DriverName = "Jim";
            driver_Jim.CarRegistration = "VSC 86";

            Driver driver_David = new Driver();
            driver_David.Id = 5;
            driver_David.DriverName = "David";
            driver_David.CarRegistration = "BFS 1L";

            Driver driver_Sian = new Driver();
            driver_Sian.Id = 6;
            driver_Sian.DriverName = "Sian";
            driver_Sian.CarRegistration = "CSG 773S";


            //Create and show the list for the menu, adding the description, price and vegetarian type to the list 
            ListViewItem food = new ListViewItem(tomatoPizza.Description);
            food.SubItems.Add(tomatoPizza.Price.ToString());
            food.SubItems.Add(tomatoPizza.IsVegetarian.ToString());
            ListViewItem food1 = new ListViewItem(peperoniPizza.Description);
            food1.SubItems.Add(peperoniPizza.Price.ToString());
            food1.SubItems.Add(peperoniPizza.IsVegetarian.ToString());
            ListViewItem food2 = new ListViewItem(bolognaise.Description);
            food2.SubItems.Add(bolognaise.Price.ToString());
            food2.SubItems.Add(bolognaise.IsVegetarian.ToString());
            ListViewItem food3 = new ListViewItem(tomatoPasta.Description);
            food3.SubItems.Add(tomatoPasta.Price.ToString());
            food3.SubItems.Add(tomatoPasta.IsVegetarian.ToString());
            listView1.Items.Add(food);
            listView1.Items.Add(food1);
            listView1.Items.Add(food2);
            listView1.Items.Add(food3);

            //Create and show the list for drivers ,adding the name, ID and car registration for each driver
            ListViewItem driver = new ListViewItem("Jim");
            driver.SubItems.Add("4");
            driver.SubItems.Add("VSC 86");
            ListViewItem driver1 = new ListViewItem("David");
            driver1.SubItems.Add("5");
            driver1.SubItems.Add("BFS 1L");
            ListViewItem driver2 = new ListViewItem("Sian");
            driver2.SubItems.Add("6");
            driver2.SubItems.Add("CSG 773S");
            listView2.Items.Add(driver);
            listView2.Items.Add(driver1);
            listView2.Items.Add(driver2);

            //Create and show the list for servers, adding the name and ID for each server 
            ListViewItem server = new ListViewItem("Ben");
            server.SubItems.Add("1");
            ListViewItem server1 = new ListViewItem("Louise");
            server1.SubItems.Add("2");
            ListViewItem server2 = new ListViewItem("Neil");
            server2.SubItems.Add("3");
            listView3.Items.Add(server);
            listView3.Items.Add(server1);
            listView3.Items.Add(server2);

            //create and show the list for the tables
            ListViewItem table1 = new ListViewItem("1");
            ListViewItem table2 = new ListViewItem("2");
            ListViewItem table3 = new ListViewItem("3");
            ListViewItem table4 = new ListViewItem("4");
            ListViewItem table5 = new ListViewItem("5");
            ListViewItem table6 = new ListViewItem("6");
            ListViewItem table7 = new ListViewItem("7");
            ListViewItem table8 = new ListViewItem("8");
            ListViewItem table9 = new ListViewItem("9");
            ListViewItem table10 = new ListViewItem("10");
            listView4.Items.Add(table1);
            listView4.Items.Add(table2);
            listView4.Items.Add(table3);
            listView4.Items.Add(table4);
            listView4.Items.Add(table5);
            listView4.Items.Add(table6);
            listView4.Items.Add(table7);
            listView4.Items.Add(table8);
            listView4.Items.Add(table9);
            listView4.Items.Add(table10);



        }

        private void button1_Click(object sender, EventArgs e)
        {
            //Call the bill form
            Bill win1 = new Bill();
            //create the totalPrice
            int totalPrice = 0;

            //shows the selected server in the bill window
            foreach (ListViewItem item in this.listView3.CheckedItems)
            {
                win1.TextBoxText += "Server Name :" + item.Text + Environment.NewLine;
            }

            //Shows the selected table in the bill window
            foreach (ListViewItem item in this.listView4.CheckedItems)
            {
                win1.TextBoxText += "Table :" + item.Text + Environment.NewLine;
            }
            //Shows the selected dishes in the bill window
            foreach (ListViewItem item in this.listView1.CheckedItems)
            {
                win1.TextBoxText += "Dishes :" + item.Text + Environment.NewLine;
                //Counts the total price
                totalPrice += Int32.Parse(item.SubItems[1].Text.ToString());
            }
            //Shows the total price in the bill window
            win1.TextBoxText += "Total price:" + totalPrice;
            win1.Show();

            orders.Add(win1.TextBoxText);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //Call the bill form
            Bill win1 = new Bill();
            //create the totalPrice
            int totalPrice = 0;
            //checks if customer name textbox is not empty
            {
                if (CustomerName.Text.Trim().Length > 0)
                {
                    //show the customer name in the bill window
                    win1.TextBoxText += "Customer Name :" + CustomerName.Text + Environment.NewLine;
                }
                else
                {
                    MessageBox.Show("Customer Name cannot be blank");
                }

                {
                    //checks if the delivery address is not empty
                    if (DeliveryAddress.Text.Trim().Length > 0)
                    {
                        //show the delivery address in the bill window
                        win1.TextBoxText += "Delivery Address :" + DeliveryAddress.Text + Environment.NewLine;
                    }
                    else
                    {
                        MessageBox.Show("Delivery Address cannot be blank");
                    }
                    //shows the selected driver in the bill window
                    foreach (ListViewItem item in this.listView2.CheckedItems)
                    {
                        win1.TextBoxText += "Driver Name :" + item.Text + Environment.NewLine;
                    }
                    //Shows the selected dishes in the bill window
                    foreach (ListViewItem item in this.listView1.CheckedItems)
                    {
                        win1.TextBoxText += "Dishes :" + item.Text + Environment.NewLine;
                        //Counts the total price
                        totalPrice += Int32.Parse(item.SubItems[1].Text.ToString());
                    }

                    //adds the 15% in the total price and shows it in the bill window
                    win1.TextBoxText += "Total price:" + totalPrice * 1.15;
                    win1.Show();
                    orders.Add(win1.TextBoxText);
                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //shows the list with all sit-in orders
            var listOrders = String.Join(", ", orders);
            MessageBox.Show(listOrders);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            //opens the order window
            Manager win2 = new Manager();
            win2.Show();
        }
    }
}