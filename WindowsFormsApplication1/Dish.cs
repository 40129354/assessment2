﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{
    class Dish
    {
        private string description;
        private bool isVegetarian;
        private int price;
        //takes the validation for the price
        public int Price
        {
            get { return price; }
            set
            {
                if (value > 0 && value < 100000) { price = value; }
            }
        }//takes the validation for description
        public string Description
        {
            get { return description; }
            set
            {
                if (value == null) throw new ArgumentNullException();
                description = value;
            }
        }
        public bool IsVegetarian
        {
            get { return isVegetarian; }
            set
            {
                isVegetarian = value;
            }
        }
    }
}
