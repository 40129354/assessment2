﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Manager : Form
    {
        public Manager()
        {
            InitializeComponent();

            // Create dish, servers and drivers when Form1 is called taking the validations from the classes.
            Dish tomatoPizza = new Dish();
            tomatoPizza.Description = "Tomato Pizza";
            tomatoPizza.Price = 450;
            tomatoPizza.IsVegetarian = true;

            Dish peperoniPizza = new Dish();
            peperoniPizza.Description = "Peperoni Pizza";
            peperoniPizza.IsVegetarian = false;
            peperoniPizza.Price = 500;

            Dish bolognaise = new Dish();
            bolognaise.Description = "Spaghetti Bolognaise";
            bolognaise.IsVegetarian = false;
            bolognaise.Price = 600;

            Dish tomatoPasta = new Dish();
            tomatoPasta.Description = "Tomato Pasta";
            tomatoPasta.IsVegetarian = true;
            tomatoPasta.Price = 350;

            Server server_Ben = new Server();
            server_Ben.Id = 1;
            server_Ben.Name = "Ben";

            Server server_Louise = new Server();
            server_Louise.Id = 2;
            server_Louise.Name = "Louise";

            Server server_Neil = new Server();
            server_Neil.Id = 3;
            server_Neil.Name = "Neil";

            Driver driver_Jim = new Driver();
            driver_Jim.Id = 4;
            driver_Jim.DriverName = "Jim";
            driver_Jim.CarRegistration = "VSC 86";

            Driver driver_David = new Driver();
            driver_David.Id = 5;
            driver_David.DriverName = "David";
            driver_David.CarRegistration = "BFS 1L";

            Driver driver_Sian = new Driver();
            driver_Sian.Id = 6;
            driver_Sian.DriverName = "Sian";
            driver_Sian.CarRegistration = "CSG 773S";


            //Create and show the list for the menu, adding the description, price and vegetarian type to the list 
            ListViewItem food = new ListViewItem(tomatoPizza.Description);
            food.SubItems.Add(tomatoPizza.Price.ToString());
            food.SubItems.Add(tomatoPizza.IsVegetarian.ToString());
            ListViewItem food1 = new ListViewItem(peperoniPizza.Description);
            food1.SubItems.Add(peperoniPizza.Price.ToString());
            food1.SubItems.Add(peperoniPizza.IsVegetarian.ToString());
            ListViewItem food2 = new ListViewItem(bolognaise.Description);
            food2.SubItems.Add(bolognaise.Price.ToString());
            food2.SubItems.Add(bolognaise.IsVegetarian.ToString());
            ListViewItem food3 = new ListViewItem(tomatoPasta.Description);
            food3.SubItems.Add(tomatoPasta.Price.ToString());
            food3.SubItems.Add(tomatoPasta.IsVegetarian.ToString());
            listView1.Items.Add(food);
            listView1.Items.Add(food1);
            listView1.Items.Add(food2);
            listView1.Items.Add(food3);

            //Create and show the list for servers, adding the name and ID for each server 
            ListViewItem server = new ListViewItem("Ben");
            server.SubItems.Add("1");
            ListViewItem server1 = new ListViewItem("Louise");
            server1.SubItems.Add("2");
            ListViewItem server2 = new ListViewItem("Neil");
            server2.SubItems.Add("3");
            listView2.Items.Add(server);
            listView2.Items.Add(server1);
            listView2.Items.Add(server2);

            //Create and show the list for drivers ,adding the name, ID and car registration for each drive
            ListViewItem driver = new ListViewItem("Jim");
            driver.SubItems.Add("4");
            driver.SubItems.Add("VSC 86");
            ListViewItem driver1 = new ListViewItem("David");
            driver1.SubItems.Add("5");
            driver1.SubItems.Add("BFS 1L");
            ListViewItem driver2 = new ListViewItem("Sian");
            driver2.SubItems.Add("6");
            driver2.SubItems.Add("CSG 773S");
            listView3.Items.Add(driver);
            listView3.Items.Add(driver1);
            listView3.Items.Add(driver2);


        }

        private void button1_Click(object sender, EventArgs e)
        {
            //If the textboxes contain text
            if (textBox1.Text.Trim() != "" && textBox2.Text.Trim() != "" && textBox3.ToString() != "")
            {
                //Then add the new dish in the list
                listView1.Items.Add(textBox1.Text).SubItems.Add(textBox2.Text);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem item in this.listView1.CheckedItems)
            {
                listView1.Items.Remove(item);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //If the textboxes contain text
            if (textBox4.Text.Trim() != "" && textBox5.Text.Trim() != "")
            {
                //Then add the new server in the list
                listView2.Items.Add(textBox4.Text).SubItems.Add(textBox5.Text);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            //Removes the selected server from the list 
            foreach (ListViewItem item in this.listView2.CheckedItems)
            {
                listView2.Items.Remove(item);
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            //If the textboxes contain text
            if (textBox6.Text.Trim() != "" && textBox7.Text.Trim() != "" && textBox8.Text.Trim() != "")
            {
                //Then add the new driver in the list
                listView3.Items.Add(textBox6.Text).SubItems.Add(textBox7.Text);
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            //Removes the selected driver from the list
            foreach (ListViewItem item in this.listView3.CheckedItems)
            {
                listView3.Items.Remove(item);
            }
        }
    }
}
