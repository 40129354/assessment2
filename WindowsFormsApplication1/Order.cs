﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{
    class Order
    {
        private List<Dish> menuItems = new List<Dish>();

        public void AddDish(Dish dish)
        {
            menuItems.Add(dish);
        }
    }

    class SitIn : Order
    {
        private int table;
        private string server;
        //Takes the validation for the tables
        public int Table
        {
            get { return table; }
            set
            {
                if (value < 0 && value > 10)
                {
                    throw new ArgumentException("Tables must be 10!");
                }
                else
                {
                    table = value;
                }
                try
                {
                    Table = 11;
                }
                catch (Exception excep)
                {
                    Console.WriteLine(excep.Message);
                }
            }
        }
        
        public string Server
        {
            get
            { return server; }

            set
            { server = value; }
        }

    }

    class Delivery : Order
    {
        private string customerName;
        private string deliveryAddress;
        private string driver;
        //Validation for the Customer Name
        public string CustomerName
        {
            get { return customerName; }
            set
            {
                if (value == null) throw new ArgumentNullException("Customer Name cannot be blank");
                customerName = value;
            }
        }
        //validation for the delivery address
        public string DeliveryAddress
        {
            get { return deliveryAddress; }
            set
            {
                if (value == null) throw new ArgumentNullException("Delivery Address cannot be blank");
                deliveryAddress = value;
            }
        }

        public string Driver
        {
            get { return driver; }
            set
            { driver = value; }
        }
    }
}